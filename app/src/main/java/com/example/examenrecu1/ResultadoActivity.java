package com.example.examenrecu1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class ResultadoActivity extends AppCompatActivity {
    Bundle datos;
    int numero;
    ListView listview;
    Button btnRegresar;
    String[] resultados;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        datos = getIntent().getExtras();
        numero = datos.getInt("numero");
        listview = (ListView) findViewById(R.id.lstResutados);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        resultados = new String[10];

        for (int i = 0; i < 10; i++){
            resultados[i] = numero + " * " + (i+1) + " = " + (numero * (i+1));
        }

        ArrayAdapter<String> Adaptador = new
                ArrayAdapter<String>(ResultadoActivity.this,android.R.layout.simple_expandable_list_item_1, resultados);
        listview.setAdapter(Adaptador);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
